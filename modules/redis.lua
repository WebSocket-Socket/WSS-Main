local _M = {}
local _R = {}
local encode = cloud.json.encode
local decode = cloud.json.decode

_M.requires = {}

local function safer(str)
  return (str or 'Unknown')
end

_M.load = function(WSS)
  --Create and attach redis instances to our object
  local redPub,err = cloud.redis.new()
  local redSub,err = cloud.redis.new()
  if not redSub or not redPub then 
    WSS:debug("redis make: "..err)
    WSS:event({type='system',target='critical',data='Failed to create redis instance'})
    return nil
  end
  local userID = WSS:getConfig('core').userID
  local red = setmetatable({
      _sub = redSub,
      _pub = redPub,
      _sock = sock,
      _queue = {},
      _defaultChannels = {'global','system',userID}
      },{__index = _R})
  setmetatable(_R,{__index = red._pub})

  return red
end
--Redis pub/sub module. 
--This function returns two redis connected instances. One for publish and one for subscribe.
--The wrapper functions are automatically passed to the proper redis copy.
--red:publish
--red:close
--red:pjoin
--red:join
--red:pleave
--red:leave
--red:read <-- Automatic
--defaultChannels = table array of channel names
--timeout - timeout
--poolsize
_M.start = function(WSS)
  local redPub,redSub,err,res,str
  local mod = WSS['redis']
  local config = WSS:getConfig('redis')
  local chansUser = config.defaultChannels
  local chansMod = mod._defaultChannels

  mod._pub:set_timeout(5000)
  mod._sub:set_timeout(5000)
  local res,err = mod._pub:connect('127.0.0.1',6379)
  if not res then
    mod:event({type='system',target='critical',data=err})
    mod:debug("redPub connect: "..err)
    return nil,err
  end
  res,err = mod._sub:connect('127.0.0.1',6379)
  if not res then
    mod:event({type='system',target='critical',data=err})
    mod:debug("redSub connect: "..err)
    return nil,err
  end

  local name
  if chansUser then
    for i=1,#chansUser do
      name=chansUser[i]
      mod:join(name)
    end
  end

  for i=1,#chansMod do
    name=chansMod[i]
    mod:join(name)
  end

  return true,nil
end

-------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------

function _R:close()
  self._sub:unsubscribe()
  self._sub:quit()
  self._pub:quit()
  local ok,err = self._pub:set_keepalive(0,100)
  if not ok then 
    self:debug("Failed to set keepalive for pub socket")
    ok,err = self._pub:close()
    if not ok then
      self:debug("Failed to close pub socket")
    end
  end
  local ok,err = self._sub:set_keepalive(0,100)
  if not ok then 
    self:debug("Failed to set keepalive for sub socket")
    ok,err = self._pub:close()
    if not ok then
      self:debug("Failed to close sub socket")
    end
  end
end

function _R:pjoin(channel)
  self._queue[#self._queue+1] = function() local res,err = self._sub:psubscribe(channel); return res,err end
end

function _R:join(channel)
  self._queue[#self._queue+1] = function() local res,err = self._sub:subscribe(channel); return res,err end
end

function _R:pleave(channel)
  self._queue[#self._queue+1] = function () local res,err = self._sub:punsubscribe(channel); return res,err end
end

function _R:leave(channel)
  self._queue[#self._queue+1] = function () local res,err = self._sub:unsubscribe(channel); return res,err end
end

function _R:publish(channel,msg)
  local data = self:msgOut(channel,msg)
  local res,err = self._pub:publish(channel,data)
  self:debug("Sent to redis")
  self:debug(data)
  if err then
    self:error('Redis publish error: '..safer(err),true)
    return nil,err 
  end
  return res,nil
end

function _R:read()
  local input,queue,count,channel,act
  queue = self._queue
  if queue then
    count = #queue
    for i=1,count do
      act = queue[i]
      local res,err = act()
      if not res then 
        self:error("redSub error: "..(err or 'Unknown'))
      end
      queue[i] = nil
    end
  end
  self:debug("Checking redis for replies")
  local res,err = self._sub:read_reply()
  if err and err ~= 'timeout' then --Timeouts are OK
    self:error('Redis read error: '..safer(err),true)
  elseif res then
    input = res[3]
    self:debug("Got data from redis")
    self:debug(input)
  end
  return input,nil
end

--Catch these so they get passed to the 'sub' instance properly
_R.subscribe = _R.join
_R.psubscribe = _R.pjoin
_R.punsubscribe = _R.pleave
_R.unsubscribe = _R.leave  

-------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------

_M.loop = function(mod,WSS)
  mod:tick()
  local res, err = mod:read() --Returns pre-formatted
  if err then
    return nil,err --Error will shut down the redis loop
  end
  return res,nil
end


return _M