local _M = {}
local _S = {}

_M.requires = {}

_M.load = function(WSS)
  local server = require("resty.websocket.server")
  local ws,err = server:new({timeout = 10000,max_payload_len = 2048})
  if err then WSS:debug("Socket error: "..err); return nil,err end
  local sock = setmetatable({
      ws = ws,
      run = true,
      },{__index = _S})
  return sock,nil
end

_M.start = function(WSS)

end

_M.stopping = function(WSS)
  WSS['core'].ws:send_close(1008,'Connection closed')
end

_M.loop = function(mod,WSS)
  local data,typ,err,bytes,err
  local ws = mod.ws
  bytes = 1 --If this becomes nil, a request failed somewhere
  mod:debug("Checking socket for data")
  mod:tick()
  data,typ,err = ws:recv_frame()
  if ws.fatal then
    mod:error('Fatal connection error',true)
    return nil,'Fatal connection error'
  elseif not data then 
    mod:debug("No data. Pinging. Are you alive?")
    bytes, err = ws:send_ping() --No data, try to ping
    return {target='idle',data=cloud.time.now()},nil
  elseif typ == "close" then 
    mod:debug("Client requested close")
    return nil,"Client requested close"
  elseif typ == "pong" then 
    mod:debug("Client responded with pong")
    return {target='pong',data=cloud.time.now()},nil
  elseif typ == "ping" then 
    mod:debug("Got ping. Sending pong.")
    bytes, err = ws:send_pong() --Got ping, respond with pong
    return {target='ping',data=cloud.time.now()},nil
  elseif typ == "text" then --Got text response.
    mod:debug("Got raw data")
    mod:debug(data)
    return data,nil
  else return nil,'Unknown status' --Exit to be safe.
  end

  if not bytes or err then 
    mod:error('Connection error',true)
    return nil,'Closed'
  end
end

local function SEND(func,caller,target,msg)
  local core = func.mod
  if not core then
    caller:error("Method is broken, can't SEND.")
    return
  end
  if not msg or not target then
    core:error("Can't send without target and message")
  end
  local data = caller:msgOut(target,msg)
  if not data then return end --Msgout dropped the request already

  local bytes,err = core.ws:send_text(data)
  if not bytes then
    core:error('Failed to send data',true)
  end
end

_M.attachments = {['SEND'] = SEND}

return _M