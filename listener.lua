local on = setmetatable({},{__index = function(t,k,v) rawset(t,k,{}); return t[k] end})
----------------------------------------------------------------------------------
----------------------------------------------------------------------------------
----------------------------------------------------------------------------------
----------------------------------------------------------------------------------
----------------------------------------------------------------------------------


--Listeners are functions in a table hierarchy.
--Formatted as: on.<namespace>.<target>

--Note: Besides the system namespace, these are all incoming data listeners.
--They do not catch outgoing data. Whether it's from redis or the websocket.

--The _all target is a catchall if no other listener exists for the target
--you should always have an _all listener unless you specifically want to ignore all other
--messages of that type

--If you return any value within a listener, it will be treated as an error
--and call socket:stop() automatically.

--Messages will be tagged with a field input.fromMe if they originated from this user.
--This field is used internally only and will be stripped on sending.

--Listener input table has the following fields: target, type, data, source


----------------------------------------------------------------------------------
--                             Core Listeners                                   --
--              These are generally triggered by client input                   --
----------------------------------------------------------------------------------

on.core._all = function(WSS,input) --example
  WSS['redis']:publish(input.target,input.data) --Forward all messages to redis
end

on.core.joinScene = function(WSS, input) --example
  --WSS['redis']:join(input.data)
end

on.core.joinChannel = function(WSS, input) --example
  WSS['redis']:leave(input.data)
end

on.core.leaveScene = function(WSS, input) --example
  WSS['redis']:leave(input.data)
end

on.core.leaveChannel = function(WSS, input) --example
  WSS['redis']:leave(input.data)
end

on.core.game_event = function(WSS, input) --example
end

on.core.close = function(WSS, input) --default - all
  cloud.log("Core shutting down")
end

on.core.error = function(WSS, input) --default - all
  cloud.log(input.data) --Log all errors
end

on.core.critical = function(WSS, input) --default - all
  cloud.log(input.data) --Log all errors
end

on.core.start = function(WSS, input) --default - all
  WSS['redis']:set('testA','testB')
  cloud.log({WSS['redis']:get('testA')})
  cloud.log("Core started")
  WSS['core']:SEND('start','Connected')
end

on.core.idle = function(WSS, input) --default
end

on.core.ping = function(WSS, input) --default
end

on.core.pong = function(WSS, input) --default
end

----------------------------------------------------------------------------------
--                             Redis Listeners                                  --
--                 Triggered by the redis pub/sub module                        --
----------------------------------------------------------------------------------

on.redis.global = function(WSS, input)
  WSS['redis']:SEND(input.target, input.data) --Forward the message to the user
end

on.redis.system = function(WSS, input)
  WSS['redis']:SEND(input.target, input.data) --Forward the message to the user
end

on.redis._all = function(WSS, input)
  if input.target == WSS:getConfig('core').sessionID then
    WSS['redis']:SEND('private', input-data)
  else
    WSS['redis']:SEND(input.target, input.data)
  end
end

on.redis.start = function(WSS, input) --default
  cloud.log("Redis started")
end

on.redis.close = function(WSS, input) --default
  cloud.log("Redis shutting down")
end

on.redis.error = function(WSS, input) --default
  cloud.log(input) --Log all errors
end

on.redis.critical = function(WSS, input) --default
  cloud.log(input) --Log all errors
end

----------------------------------------------------------------------------------
--                              System Listeners                                --
--                           Triggered by the loader                            --
----------------------------------------------------------------------------------

on.system.error = function(WSS, input)
  cloud.log(input.data)
end

on.system.critical = function(WSS, input)
  cloud.log(input.data) --Log all errors
end



return on