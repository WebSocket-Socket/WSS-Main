function api.get.socket(input)
--My auth stuff
  local sessionID = input.sessionID
  local res,err = OAuth.getUser(nil,sessionID)
  if err then return {error = "Access denied for session"} end
  local UUID = res.UUID
 -----
 
  local wS,err = require("txzeenath.webSockets.main").new() --Make a new WSS object
  if err then return nil,'Failed to initialize' end

  --Turn this connection into a websocket
  wS:withModule('redis')
  wS:setConfig({'redis', 'path', 'txzeenath.webSockets.modules'})
  wS:setConfig({'redis', 'DEBUG', false}) --WARNING: This will output a LOT of junk to your logs.
  wS:setConfig({'redis', 'defaultChannels', {'testA','testB'}})
  wS:setConfig({'core', 'DEBUG', false}) --WARNING: This will output a LOT of junk to your logs.
  wS:setConfig({'system', 'DEBUG', false}) --WARNING: This will output a LOT of junk to your logs.
  wS:setConfig({'core', 'listener_path', 'txzeenath.webSockets'})
  wS:setConfig({'core', 'path', 'txzeenath.webSockets.modules'})
  wS:setConfig({'core', 'sessionID', sessionID}) --Override the system generated sessionID for this connection
  wS:setConfig({'core', 'extras', 'UUID', UUID})  --Extras is a storage table. Use it for whatever values you want available under WSS['core'].extras
  wS:loadAll() --Load modules and config into object
  wS:start() --Start it up

  --We will never reach down here because once the socket closes it will terminate the request.
end