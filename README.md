Copy the repo to plugins/txzeenath/webSockets.  
  
  
It should look like:  
```
plugins:  
  txzeenath:  
    webSockets:  
      listener.lua  
      main.lua  
      modules:  
        redis.lua  
        core.lua  
     ``` 
      
      You will need redis server installed on your system. Cloud does not have it installed by default.