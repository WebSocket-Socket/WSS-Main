local _M = {}
local _N = {}
local on
local encode,decode = cloud.json.encode,cloud.json.decode
local slower,supper,pcl,ctime,ty,ps,tos,sleep,log,nxt = string.lower,string.upper,pcall,cloud.time.now,type,pairs,tostring,ngx.sleep,cloud.log,next

local disableLog = true
--Method
local function debug(func,mod,str)
  if disableLog then return end
  local config,DEBUG,name
  local WSS = func._WSS
  local config

  if WSS and mod then
    config = WSS:getConfig(mod._name)
    if config then DEBUG = config.DEBUG end
    name = mod._name
  else
    name = 'undefined'
  end

  if DEBUG or not WSS then
    log({Module=name,DEBUG=str})
  end
end

--Method
local function err (func,mod,str,critical)
  local tab = {
    ty = (mod._name or 'undefined'),
    target = ((critical and 'critical') or 'error'),
    data = str,
    source = 'undefined'
  }
  if mod then 
    mod:debug(tab) 
  else 
    func._WSS:debug(tab) 
  end
  pcl(function() on[tab.ty][tab.target](func._WSS,tab) end)
end

--Method
local function stop(func,mod)
  if mod._run then
    mod:event({target='close',data='Shutting down'})
    mod._run = false
    pcall(func._WSS._stoppers[mod._name],func._WSS)
  end
end

--Method
local function tick(func,mod)
  mod._lu = ctime()
end

--Method
local function event(func,mod,tab)
  if not tab then return end

  local WSS = func._WSS
  if not WSS[mod._name] then
    mod:error("Can't send event. Not done loading yet")
    return
  end
  if ty(tab) == 'string' then
    mod:debug("Decoding: "..tab)
    tab = mod:msgIn(tab)
    mod:debug(tab)
  end

  if not tab or ty(tab) ~= 'table' then
    mod:error('Events must be passed as tables or JSON.')
    mod:error(tab)
    return
  end

  local tyVal = mod._name or tab.ty
  local targetVal = tab.target
  if not tyVal or not targetVal then
    mod:error('Events must have a target defined!')
    return
  end
  mod:debug("Processing event")
  mod:debug(tab)
  local tyLst = on[tyVal]
  local tyLstT = ty(tyLst)
  local callback,targetLst
  if tyLst then --Listener for this ty exists
    if tyLstT == 'function' then --type listener is a function. Someone didn't read the docs
      mod:debug("Using type listener. Read the docs!")
      callback = tyLst --Use it
    elseif  tyLstT == 'table' then --type listener is a table. Good.
      targetLst = tyLst[targetVal]
      if targetLst and ty(targetLst) == 'function' then --target listener exists and it's a function
        callback = targetLst --Use it
        mod:debug("Using target listener")
      elseif tyLst._all and ty(tyLst._all) == 'function' then --fallback exists and is a function
        callback = tyLst._all --Use it
        mod:debug("Using fallback listener")
      end
    end
  end

  if not callback then --No callback, let's try to let them know. Unless of course there's no error listener either.
    mod:error('Unsupported callback: <'..(tos(tab.ty) or 'none')..'>.<'..(tos(tab.target) or 'none')..'>')
  else
    local err = callback(WSS,tab)
    if err then
      mod:debug("Error returned from listener: "..tos(err))
      mod:stop()
    end
  end
end


--Encoded fixed format string
--For output
local function msgOut(func,mod,target,data)
  if not data or not target then
    mod:error('msgOut requires a target and data')
    return nil
  end
  local sessionID = func._WSS:getConfig('core').sessionID
  local newTable = {}
  newTable[1] = data --req
  newTable[2] = target --req
  newTable[3] = mod._name --auto
  newTable[4] = sessionID --auto
  local ok, res = pcl(encode, newTable)
  local output
  if not ok then
    mod:error('Could not encode message')
  else
    output = res
  end

  return output
end

--Decoded fixed format table
--For input
local function msgIn(func,mod,msgString)
  local ok,res = pcl(decode, msgString)
  local input
  if not ok then
    mod:error('Could not decode message')
  else
    input = {
      data = res[1], --req
      target = res[2] or 'undefined', -- req
      ty = mod._name or res[3], --auto
      source = res[4] or 'undefined' --auto
    }
  end
  if input then
    input.ty = mod._name
    if input.source == func._WSS:getConfig('core').sessionID then 
      input.fromMe = true --This is only used in the cloud for filtering
    end
  end
  return input
end

local error_mt = {__call = err}
local debug_mt = {__call = debug}
local event_mt = {__call = event}
local stop_mt = {__call = stop}
local tick_mt = {__call = tick}
local msgOut_mt = {__call = msgOut}
local msgIn_mt = {__call = msgIn}
function _N:attachMod(mod)
  --Attach standard methods
  --All module objects get access to these.
  mod.error = setmetatable({_WSS = self},error_mt)
  mod.debug = setmetatable({_WSS = self},debug_mt)
  mod.event = setmetatable({_WSS = self},event_mt)
  mod.stop = setmetatable({_WSS = self},stop_mt)
  mod.tick = setmetatable({_WSS = self},tick_mt)
  mod.msgOut = setmetatable({_WSS = self},msgOut_mt)
  mod.msgIn = setmetatable({_WSS = self},msgIn_mt)
end

--Create loader object
_M.new = function()
  local new = setmetatable({
      ['_name'] = 'system',
      ['_modules'] = {
        ['core'] = true
      },
      ['_starters'] = {
      },
      ['_stoppers'] = {
      },
      ['_loops'] = {
      },
      ['_loaders'] = {
      },
      ['_attachments'] = {
      },
      ['_config'] = {
        ['core'] = {
          ['path'] = 'txzeenath.webSockets.modules',
          ['DEBUG'] = false,
          ['listener_path'] = 'txzeenath.webSockets',
          ['sessionID'] = cloud.uuid()
        }
      }
      },{__index = _N})
  new:attachMod(new) --System gets access to the same functions
  return new
end

function _N:getConfig(name)
  name = slower(name)
  if self._config and self._config[name] then
    return self._config[name]
  end
  return nil,'No configuration available'
end

local function makeNode(table,key)
  if not key then return table end
  if not table[key] then table[key] = {} end
  return table[key]
end

function _N:setConfig(arr)
  if not arr or not nxt(arr) then
    self:debug("NIL parameter for setConfig!")
    return
  end
  arr[1] = slower(arr[1])
  if not self._config then self._config = {} end

  local tab = self._config
  local prevTab
  for i=1,#arr-1 do
    prevTab = tab
    tab = makeNode(tab,arr[i])
  end
  prevTab[arr[#arr-1]] = arr[#arr]
  if arr[#arr-1] == 'DEBUG' and arr[#arr] then disableLog = false end
  self:debug("Set config: "..cloud.json.encode(self._config))
end

function _N:withModule(str)
  str = slower(str)
  self._modules[str] = true
end

function _N:loadModule(name,path)
  self:debug("Loading "..name.." from "..path)
  local ok,mod = pcl(require,path..'.'..name)
  local loader,starter,loop,loaded,err,stopper
  if ok then
    loader = mod.load --Object creator
    starter = mod.start --Connector
    stopper = mod.stopper --Stopping
    loop = mod.loop --Main loop
    if loader then
      loaded,err = mod.load(self) --Initializer
    end
    if loaded and starter and loop and loader and not err then
      self._modules[name] = loaded --Store the loaded module object
      self._starters[name] = starter --Store startup function
      self._stoppers[name] = stopper
      self._loops[name] = loop --Store loop function
      self._modules[name]._name = name --Attach name value
      self._modules[name]._run = true --attach run value
      self._attachments[name] = mod.attachments
      self:attachMod(self._modules[name]) --Attach methods
      self._modules[name]:tick() --Tick once when starting
      self._modules[name]:debug("Loaded module")
    end
  else
    self:error("Failed to require: "..tos(mod))
  end

  if not ok or not loaded or not starter or not loop or not loader or err then
    self._modules[name] = nil
    self:error("Failed to load module: "..name)
  end
end

--Initialize modules
function _N:loadAll()
  local ok,listener = pcl(require,self._config.core.listener_path..".listener")
  if not ok then 
    self:debug("Failed to load listener")
    self:debug(listener)
    return ngx.exit(444)
  end
  self._onhook = listener --Add a hook so we can add pre-processor events
  on = listener
  local path,hasConfig
  for i,v in ps(self._modules) do
    if v then
      hasConfig = self._config[i]
      if hasConfig then
        path = hasConfig.path
        if not path then
          path = self._config['core']['path']
        end
        if not path then
          self:debug("Could not find path variable for: "..i)
          self._modules[i] = nil
        else
          self:loadModule(i,path)
        end
      end
    end
  end
  self:debug("Loading modules done")
  self:debug("Attaching module methods")
  for i,v in ps(self._modules) do
    if v then
      local attachments = self._attachments[i]
      if attachments then
        for method,func in ps(attachments) do
          if ty(func) == 'function' then
            for modname,v in ps(self._modules) do
              if v then
                self._modules[i]:debug("Attaching "..method.." to "..modname)
                self._modules[modname][supper(method)] = setmetatable({mod = self._modules[i]},{__call = func})
              end
            end
          else
            self._modules[i]:error('Can only use functions as attachments. '..tos(method)..' is not valid.')
          end
        end
      end
    end
  end

end

local function startLoop(WSS,name)
  local mod = WSS._modules[name]
  local core = WSS._modules.core
  local loop = WSS._loops[name]
  local time,res,err
  while core._run and mod._run do
    time = ctime()-30
    if mod._lu < time or core._lu < time then break end --Idle too long. Close.
    res,err = loop(mod,WSS)
    if err then break end
    if res then mod:event(res) end
    sleep(0.002)
  end
  mod:stop()
  return nil,err
end

local function dummy(...)
end
local errorCatchFunc_mt = {
  __call = function() return dummy end, --If it's a function call, give it a function
  __index = function() return {} end, --If it's a table, give it something to use
  __newindex = function() return {} end
}
local errorCatchFunc = setmetatable({},errorCatchFunc_mt)
local errorCatch_mt = {
  __index = function(t,k,v) cloud.log("ERROR: Attempt to call: "..tos(k).." on unloaded module") return errorCatchFunc end, --Catch bad get index
  __newindex = function(t,k,v) cloud.log("ERROR: Attempt to call: "..tos(k).." on unloaded module") return errorCatchFunc end --Catch bad set index
}
local errorCatch = setmetatable({},errorCatch_mt)
local notLoaded_mt = {
  __index = function(t,k,v) cloud.log("Module is not loaded: "..tos(k)) return errorCatch end, --Catch bad get index
  __newindex = function(t,k,v) cloud.log("Module is not loaded: "..tos(k)) return errorCatch end --Catch bad set index
}

function _N:start()
  if not self._modules.core then
    self:error("Core module is not loaded. Cannot continue",true)
    return ngx.exit(444)
  end
  local threads = {}
  setmetatable(_N,{__index = self._modules}) --If key doesn't exist, see if it's a module. Allows WSS['redis']
  setmetatable(self._modules,notLoaded_mt) --If we try to index an invalid module, let the error table catch it
  local threadCount=0
  for i,v in ps(self._modules) do
    if v then
      local ok,res,err = pcl(self._starters[i],self) --Run startup function
      if err or not ok then
        self._modules[i]:error("Startup failed: "..(err or res or 'Unknown'),true)
        self._modules[i] = nil
      else
        self._modules[i]:debug("Starting thread")
        threads[threadCount+1] = cloud.async.start(function () startLoop(self,i) end)
        if not threads[threadCount+1] then
          v:debug("Failed to start thread")
          self._modules[i] = nil
        else
          v:debug("Started thread")
          threadCount=threadCount+1
        end
      end
    end
  end

  --Send out start event for all modules
  for i,v in ps(self._modules) do
    if v then
      v:event({target='start',data=ctime})
    end
  end

  local res,err = cloud.async.wait(threads)
  local mod
  for i,v in ps(self._modules) do
    if v then
      v:debug("Stopping")
      v:stop()
    end
  end

  sleep(1)
  return ngx.exit(444)
end



return _M
